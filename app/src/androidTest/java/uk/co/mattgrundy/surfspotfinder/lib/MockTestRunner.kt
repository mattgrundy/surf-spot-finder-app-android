package uk.co.mattgrundy.surfspotfinder.lib

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner

/**
 * Launches TestApplication as test application environment
 * Attached in app gradle build file
 */
class MockTestRunner : AndroidJUnitRunner() {
    override fun newApplication(
        cl: ClassLoader?,
        className: String?,
        context: Context?
    ): Application {
        return super.newApplication(cl, TestApplication::class.java.getName(), context)
    }
}