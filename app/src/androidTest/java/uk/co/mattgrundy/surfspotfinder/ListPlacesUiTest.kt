package uk.co.mattgrundy.surfspotfinder

import android.view.View
import androidx.fragment.app.FragmentFactory
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.jakewharton.espresso.OkHttp3IdlingResource
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.CoreMatchers.containsString
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import uk.co.mattgrundy.surfspotfinder.lib.dagger.components.DaggerTestAppComponent
import uk.co.mattgrundy.surfspotfinder.lib.dagger.components.TestAppComponent
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.PlaceLocation
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.PlaceLocationContainer
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.PlaceResult
import uk.co.mattgrundy.surfspotfinder.lib.data.viewmodel.PlacesViewModel
import uk.co.mattgrundy.surfspotfinder.lib.repository.network.MockServerDispatcher
import uk.co.mattgrundy.surfspotfinder.ui.fragments.SurfSpotDetailFragment
import uk.co.mattgrundy.surfspotfinder.ui.fragments.SurfSpotFinderContainerFragment
import javax.inject.Inject

@RunWith(AndroidJUnit4::class)
class ListPlacesUiTest {

    @Inject
    lateinit var okHttpClient: OkHttpClient

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var testAppComponent: TestAppComponent

    lateinit var idlingResource: IdlingResource

    private var mockWebServer: MockWebServer = MockWebServer()

    lateinit var fragmentFactory: FragmentFactory

    @Before
    fun setup() {
        // create our Dagger test component and inject this test
        testAppComponent = DaggerTestAppComponent.builder()
            .application(ApplicationProvider.getApplicationContext())
            .build()
        fragmentFactory = testAppComponent.fragmentFactory()
        testAppComponent.inject(this)

        // create our idling resource to wait for network requests
        idlingResource = OkHttp3IdlingResource.create("OkHttp", okHttpClient)

        // start the mock server
        mockWebServer.start(8080)
        mockWebServer.dispatcher = MockServerDispatcher()
    }

    @After
    fun tearDown() {
        // stop the mock server
        mockWebServer.shutdown()
    }

    @Test
    fun testListingResults() {
        IdlingRegistry.getInstance().register(idlingResource)

        // launch our fragment, use the FragmentFactory to create
        // this to satisfy injected constructor
        launchFragmentInContainer<SurfSpotFinderContainerFragment>(
            null,
            R.style.AppTheme,
            fragmentFactory
        )

        val view = withId(R.id.places_list)

        // check the list is displayed
        onView(view).check(matches(isDisplayed()))
        
        // check we've got "Plymouth" at position 0
        onView(withId(R.id.places_list))
            .check(
                matches(
                    atPosition(0, hasDescendant(withText("Plymouth")))
                )
            )

        // check we've got "The Duke of Cornwall Hotel" at position 1
        onView(withId(R.id.places_list))
            .check(
                matches(
                    atPosition(1, hasDescendant(withText("The Duke of Cornwall Hotel")))
                )
            )

        IdlingRegistry.getInstance().unregister(idlingResource)
    }

    @Test
    fun testShowPlaceDetails() {
        IdlingRegistry.getInstance().register(idlingResource)

        val placeResult = PlaceResult(
            "a4c03b48ee7a01740de51824fb14383c848609bc",
            "ChIJxQewSXzpbEgRT90gX9yOWKc",
            "Newton Ferrers Equus",
            "https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png",
            PlaceLocationContainer(PlaceLocation(50.32638300000001, -4.029798)),
            null,
            null,
            null,
            null,
            null,
            null
        )

        val placesViewModel = viewModelFactory.create(PlacesViewModel::class.java)
        placesViewModel.selectedPlaceItem.postValue(placeResult)

        // launch our fragment, use the FragmentFactory to create
        // this to satisfy injected constructor
        launchFragmentInContainer<SurfSpotDetailFragment>(
            null,
            R.style.AppTheme,
            fragmentFactory
        )

        // check we've got "Newton Ferrers Equus" displayed
        onView(withId(R.id.text_title))
            .check(
                matches(withText(containsString("Newton Ferrers Equus")))
            )

        // check we've got the address displayed
        onView(withId(R.id.text_address))
            .check(
                matches(withText(containsString("Newton Downs Farm, Newton Ferrers, Plymouth PL8 1JA, UK")))
            )

        IdlingRegistry.getInstance().unregister(idlingResource)
    }


    /**
     * Find child view matching matcher
     * @param position view index
     * @param itemMatcher view matcher
     */
    private fun atPosition(
        position: Int,
        itemMatcher: Matcher<View?>
    ): Matcher<View?>? {
        return object : BoundedMatcher<View?, RecyclerView>(RecyclerView::class.java) {
            override fun describeTo(description: Description) {
                description.appendText("has item at position $position: ")
                itemMatcher.describeTo(description)
            }

            override fun matchesSafely(view: RecyclerView): Boolean {
                val viewHolder =
                    view.findViewHolderForAdapterPosition(position)
                        ?: // has no item on such position
                        return false
                return itemMatcher.matches(viewHolder.itemView)
            }
        }
    }


}