package uk.co.mattgrundy.surfspotfinder

import androidx.test.platform.app.InstrumentationRegistry
import junit.framework.Assert.*
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import uk.co.mattgrundy.surfspotfinder.lib.TestApplication
import uk.co.mattgrundy.surfspotfinder.lib.dagger.components.DaggerTestAppComponent
import uk.co.mattgrundy.surfspotfinder.lib.dagger.components.TestAppComponent
import uk.co.mattgrundy.surfspotfinder.lib.data.models.location.SearchParams
import uk.co.mattgrundy.surfspotfinder.lib.repository.network.MockServerDispatcher
import uk.co.mattgrundy.surfspotfinder.lib.repository.network.repositories.PlacesRepository
import javax.inject.Inject

/**
 * Testing the networking layer - Retrofit -> OkHttp -> Repository
 * Note: using CountDownLatch can cause delays
 */
class NetworkRequestsTest {

    private var mockWebServer: MockWebServer = MockWebServer()

    @Inject
    lateinit var placesRepository: PlacesRepository

    private lateinit var testAppComponent: TestAppComponent

    @Before
    fun setup() {
        // start the server
        mockWebServer.start(8080)
        mockWebServer.dispatcher =
            MockServerDispatcher()

        val context = InstrumentationRegistry.getInstrumentation()
            .targetContext.applicationContext

        val app = context as TestApplication
        testAppComponent = DaggerTestAppComponent.builder()
            .application(app)
            .build()
        app.appComponent = testAppComponent

        testAppComponent.inject(this)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun testPlacesListWebResponse() {
        runBlocking {
            // results will be returned from static JSON file repo_place_list
            val response = placesRepository.getPlacesNearby(
                "surf",
                SearchParams.SearchOrder.DISTANCE,
                52.1, -3.5, 10
            )

            val placesContainer = response.body
            val success = response.success

            assertNotNull(
                "Check result items are not null",
                placesContainer
            )

            assertTrue(
                "Check network returned success response",
                success
            )

            assertEquals(
                "Check number of places returned",
                20,
                placesContainer?.results?.size
            )

            // asserted not null
            val firstPlace = placesContainer?.results?.get(0)

            assertEquals(
                "Check first returned place ID",
                "147cc55720e35b5c21fece2b63a7e41922de2924",
                firstPlace?.id
            )

            val secondPlace = placesContainer?.results?.get(1)

            assertEquals(
                "Check second returned place ID",
                "e7c55c513815d71c85b65816806132cc0c75ddfb",
                secondPlace?.id
            )
        }

        mockWebServer.takeRequest()
    }

    @Test
    fun testPlacesDetailWebResponse() {
        runBlocking {
            // results will be returned from static JSON file repo_place_detail
            val response = placesRepository.getPlaceById(
                "ChIJqQpj7E-TbEgRBzvdQl9-ySw"
            )

            val success = response.success
            val placeContainer = response.body

            assertTrue(
                "Check network returned success response",
                success
            )

            assertNotNull(
                "Place container is not null",
                placeContainer
            )

            val placeContainerInstance = placeContainer!!

            assertEquals(
                "Check place ID returned",
                "a4c03b48ee7a01740de51824fb14383c848609bc",
                placeContainerInstance.result.id
            )

            assertEquals(
                "Check place title returned",
                "Newton Ferrers Equus",
                placeContainer.result.name
            )
        }

        mockWebServer.takeRequest()
    }

}