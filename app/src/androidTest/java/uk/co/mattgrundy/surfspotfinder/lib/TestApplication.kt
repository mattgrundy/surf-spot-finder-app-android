package uk.co.mattgrundy.surfspotfinder.lib

import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import timber.log.Timber
import timber.log.Timber.DebugTree
import uk.co.mattgrundy.surfspotfinder.lib.dagger.components.DaggerTestAppComponent
import uk.co.mattgrundy.surfspotfinder.lib.dagger.components.TestAppComponent
import javax.inject.Inject

class TestApplication : Application(), HasAndroidInjector {

    var appComponent: TestAppComponent? = null

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()

        Timber.plant(DebugTree())

        appComponent = initDagger(this)
        appComponent?.inject(this)
    }

    private fun initDagger(app: Application): TestAppComponent =
        DaggerTestAppComponent.builder()
            .application(app)
            .build()

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

}