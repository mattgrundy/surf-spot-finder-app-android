package uk.co.mattgrundy.surfspotfinder.lib.dagger.components

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import uk.co.mattgrundy.surfspotfinder.ListPlacesUiTest
import uk.co.mattgrundy.surfspotfinder.NetworkRequestsTest
import uk.co.mattgrundy.surfspotfinder.lib.TestApplication
import uk.co.mattgrundy.surfspotfinder.lib.dagger.modules.*
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        RepositoryModule::class,
        ApiModule::class,
        NetworkModule::class,
        EnvironmentModule::class,
        TestViewModelModule::class,
        FragmentInjectorModule::class,
        ListenerModule::class,
        FragmentFactoryModule::class
    ]
)

interface TestAppComponent : AppComponent {
    @Component.Builder
    interface TestBuilder {
        @BindsInstance
        fun application(application: Application): TestBuilder

        fun build(): TestAppComponent
    }

    fun inject(app: TestApplication)
    fun inject(app: NetworkRequestsTest)
    fun inject(app: ListPlacesUiTest)
}
