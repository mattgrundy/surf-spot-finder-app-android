package uk.co.mattgrundy.surfspotfinder.lib.repository.network

import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest
import timber.log.Timber

/**
 * Mock server dispatcher - returns static responses for given API requests
 */
class MockServerDispatcher : Dispatcher() {

    // array of places
    private val placeListJsonFile = "repo_place_list.json"

    // place details
    private val placeDetailJsonFile = "repo_place_detail.json"

    /**
     * Override dispached request, return values from local JSON files
     * @param request request to be made
     * @return mocked API response
     */
    override fun dispatch(request: RecordedRequest): MockResponse {
        Timber.d("Making request: " + request.path)

        return when {

            request.path.startsWith("/place/nearbysearch/json") ->
                MockResponse()
                    .setResponseCode(200)
                    .setBody(readFileContents(placeListJsonFile))

            request.path.startsWith("/place/details/json") ->
                MockResponse()
                    .setResponseCode(200)
                    .setBody(readFileContents(placeDetailJsonFile))

            else -> MockResponse().setResponseCode(404)
        }
    }

    /**
     * Read the contents of the file from resources dir
     * @param filePath Name of the file in the resources dir
     * @return contents of the file
     */
    private fun readFileContents(filePath: String): String {
        val stream = javaClass.classLoader?.getResourceAsStream(filePath)

        val sb = StringBuilder()
        stream?.let {
            val resource = it.bufferedReader()

            var line = resource.readLine()

            while (line != null) {
                sb.append(line)
                line = resource.readLine()
            }
        }

        return sb.toString()
    }

}