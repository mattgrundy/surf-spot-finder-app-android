package uk.co.mattgrundy.surfspotfinder

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import org.junit.Assert
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import uk.co.mattgrundy.surfspotfinder.lib.config.CurrentEnvironment
import uk.co.mattgrundy.surfspotfinder.lib.exceptions.InvalidEnvironmentException

/**
 * Test class for CurrentEnviroment - make sure the correct environment variables are returned based
 * on the input key
 */
class CurrentEnvironmentTest {
    /**
     * Setup the environment before each tests
     * Static variables persist throughout, so need to be manually reset
     */
    @Before
    fun beforeEach() {
        CurrentEnvironment.init()
    }

    @Test
    fun testDevKeyReturnsTestEnvironment() {
        val devEnvironment = CurrentEnvironment.getEnvironment("DEV")

        assertTrue(
            "Dev environment is asserting as a test environment",
            devEnvironment.isTestEnvironment
        )
    }

    @Test
    fun testReleaseKeyReturnsLiveEnvironment() {
        val liveEnvironment = CurrentEnvironment.getEnvironment("RELEASE")

        assertFalse(
            "Live environment is not asserting as a test environment",
            liveEnvironment.isTestEnvironment
        )
    }

    @Test
    fun testUnknownKeyThrowsException() {
        // check an unknown environment type throws an exception
        try {
            CurrentEnvironment.getEnvironment("ABC")
            Assert.fail("ABC is not a valid environment")
        } catch (e: InvalidEnvironmentException) {
            // success
        }
    }

    @Test
    fun testGettingGoogleKeyNotInitialisedException() {
        val devEnvironment = CurrentEnvironment.getEnvironment("DEV")

        try {
            devEnvironment.getImageUrl("abcd")
            Assert.fail("Current Environment has not yet been initialised")
        } catch (e: CurrentEnvironment.Environment.EnvironmentNotInitializedException) {
            // success
        }
    }

    @Test
    fun testGettingGoogleKey() {
        val context = ApplicationProvider.getApplicationContext<Context>()

        val devEnvironment = CurrentEnvironment.getEnvironment("DEV")
        devEnvironment.initEnvironmentFromContext(context)

        val googleImageUrl = devEnvironment.getImageUrl("abcd")

        // check it's been initialised
        Assert.assertNotNull(googleImageUrl)
    }
}
