package uk.co.mattgrundy.surfspotfinder

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import io.mockk.*
import io.mockk.impl.annotations.MockK
import junit.framework.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import uk.co.mattgrundy.surfspotfinder.lib.data.DummyDataFactory
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.PlaceContainer
import uk.co.mattgrundy.surfspotfinder.lib.data.viewmodel.PlacesViewModel
import uk.co.mattgrundy.surfspotfinder.lib.repository.network.repositories.PlacesRepository

class PlaceDetailsViewModelTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @MockK
    lateinit var placesRepository: PlacesRepository

    private lateinit var placesViewModel: PlacesViewModel

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    private fun createPlaceObserver(): Observer<PlaceContainer> = spyk(Observer { })

    @Test
    fun testPlaceWhenSuccessViewModel() {
        // create our mocked observer
        val mockedObserver = createPlaceObserver()

        // create our test ViewModel
        placesViewModel = PlacesViewModel(placesRepository)
        // ObserveForever to keep this instance alive
        placesViewModel.place.observeForever(mockedObserver)

        // create some test data to supply
        val placeItem = DummyDataFactory.createPlaceContainer()

        coEvery { placesRepository.getPlaceById(any()) } returns PlacesRepository.RepositoryResult<PlaceContainer>(
            true,
            null,
            placeItem
        )

        // make the call to get the details from the ViewModel -> Repository
        placesViewModel.getPlaceDetails("")

        // capture the calls to the observer to count calls
        val slots = mutableListOf<PlaceContainer>()
        verify { mockedObserver.onChanged(capture(slots)) }

        // check we've got the place result
        assertNotNull(placesViewModel.place.value)

        // check the ID of the returned item is as expected
        assertEquals(placeItem.result.id, placesViewModel.place.value?.result?.id)
    }

    @Test
    fun testPlaceWhenFailedViewModel() {
        // create our mocked observer
        val mockedObserver = createPlaceObserver()

        // create our test ViewModel
        placesViewModel = PlacesViewModel(placesRepository)
        // ObserveForever to keep this instance alive
        placesViewModel.place.observeForever(mockedObserver)

        // reply to the lambda with the test data
        coEvery { placesRepository.getPlaceById(any()) } returns PlacesRepository.RepositoryResult<PlaceContainer>(
            false,
            "Test Fail",
            null
        )

        // make the call to get the details from the ViewModel -> Repository
        placesViewModel.getPlaceDetails("")

        /*
        Removing for current bug in mockk
        // capture the calls to the observer to count calls
        val placeholderItems = mutableListOf<PlaceContainer>()

        // make sure this isn't called
        verify(exactly = 0) {
            mockedObserver.onChanged(capture(placeholderItems))
        }
        */

        // check we've got a null place
        assertNull(placesViewModel.place.value)
    }

}
