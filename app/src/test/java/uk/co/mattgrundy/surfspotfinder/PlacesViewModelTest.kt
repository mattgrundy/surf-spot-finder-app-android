package uk.co.mattgrundy.surfspotfinder

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import io.mockk.*
import io.mockk.impl.annotations.MockK
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import uk.co.mattgrundy.surfspotfinder.lib.data.DummyDataFactory
import uk.co.mattgrundy.surfspotfinder.lib.data.models.location.LocationModel
import uk.co.mattgrundy.surfspotfinder.lib.data.models.location.SearchParams
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.PlacesContainer
import uk.co.mattgrundy.surfspotfinder.lib.data.viewmodel.PlacesViewModel
import uk.co.mattgrundy.surfspotfinder.lib.repository.network.repositories.PlacesRepository

class PlacesViewModelTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @MockK
    lateinit var placesRepository: PlacesRepository

    private lateinit var placesViewModel: PlacesViewModel

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    private fun createPlaceObserver(): Observer<PlacesContainer> = spyk(Observer { })

    @Test
    fun testPlaceViewModel() {
        // create our mocked observer
        val mockedObserver = createPlaceObserver()

        // create our test ViewModel
        placesViewModel = PlacesViewModel(placesRepository)
        // ObserveForever to keep this instance alive
        placesViewModel.places.observeForever(mockedObserver)

        // create some test data to supply
        val placesItem = DummyDataFactory.createPlacesList()

        // reply to the lamda with the test data
        // reply to the lambda with the test data
        coEvery {
            placesRepository.getPlacesNearby(
                any(),
                SearchParams.SearchOrder.DISTANCE,
                any(),
                any(),
                any()
            )
        } returns PlacesRepository.RepositoryResult<PlacesContainer>(
            true,
            null,
            placesItem
        )

        // make the call to get the details from the ViewModel -> Repository
        placesViewModel.getPlacesNearby(
            "",
            SearchParams.SearchOrder.DISTANCE,
            LocationModel(52.0, -3.0),
            10
        )

        // capture the calls to the observer to count calls
        val slots = mutableListOf<PlacesContainer>()
        verify { mockedObserver.onChanged(capture(slots)) }

        // check we've got the place result
        assertNotNull(placesViewModel.places)

        // check the length of the results matches the value supplied
        assertEquals(11, placesViewModel.places.value?.results?.size)
    }

}
