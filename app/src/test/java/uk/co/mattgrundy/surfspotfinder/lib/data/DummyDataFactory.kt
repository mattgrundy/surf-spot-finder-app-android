package uk.co.mattgrundy.surfspotfinder.lib.data

import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.*

class DummyDataFactory {

    companion object {
        fun createPlace(): PlaceResult {
            return PlaceResult(
                "8c237ed486251d05591031ba065ca7eb21ee43ed",
                "ChIJqQpj7E-TbEgRBzvdQl9-ySw",
                "New Continental Hotel",
                "https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png",
                PlaceLocationContainer(PlaceLocation(50.3685448, -4.1484977)),
                null,
                null,
                null,
                null,
                null
            )
        }

        fun createPlacesList(): PlacesContainer {
            val dummyPlaces = ArrayList<PlaceResult>()
            for (x in 0..10) {
                dummyPlaces.add(createPlace())
            }
            return PlacesContainer(dummyPlaces)
        }

        fun createPlaceContainer(): PlaceContainer {
            return PlaceContainer(createPlace())
        }
    }
}