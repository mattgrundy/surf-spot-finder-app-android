package uk.co.mattgrundy.surfspotfinder.lib.data.models.place

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "place_opening_hours")
data class PlaceOpeningHours(
    var open_now: Boolean = false,

    @Ignore
    val weekday_text: List<String>? = null
)