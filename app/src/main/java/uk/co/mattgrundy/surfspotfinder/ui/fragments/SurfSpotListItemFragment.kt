package uk.co.mattgrundy.surfspotfinder.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.TransitionInflater
import dagger.android.support.DaggerFragment
import uk.co.mattgrundy.surfspotfinder.R
import uk.co.mattgrundy.surfspotfinder.databinding.SurfSpotListItemBinding
import uk.co.mattgrundy.surfspotfinder.lib.config.CurrentEnvironment
import uk.co.mattgrundy.surfspotfinder.lib.data.viewmodel.PlacesViewModel
import uk.co.mattgrundy.surfspotfinder.lib.listeners.OnListItemClickListener
import uk.co.mattgrundy.surfspotfinder.lib.transitions.IncomingTransition
import uk.co.mattgrundy.surfspotfinder.ui.adapters.SurfSpotAdapter
import javax.inject.Inject

/**
 * Fragment to show a list of places
 */
class SurfSpotListItemFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    private val currentEnvironment: CurrentEnvironment.Environment
) : DaggerFragment() {

    private val placesViewModel: PlacesViewModel by activityViewModels {
        viewModelFactory
    }

    private var viewAdapter: SurfSpotAdapter? = null
    private var viewManager: RecyclerView.LayoutManager? = null

    private var _binding: SurfSpotListItemBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = SurfSpotListItemBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // observe the places list
        placesViewModel.places.observe(viewLifecycleOwner, Observer {
            viewAdapter?.data = it.results
        })

        viewManager = LinearLayoutManager(activity)
        viewAdapter = SurfSpotAdapter(this, currentEnvironment)

        // register the onclick for the RecyclerView
        viewAdapter?.onListItemClickListener = object : OnListItemClickListener {
            override fun onItemClick(position: Int) {
                val placeItem = viewAdapter?.data?.get(position)

                placeItem?.let {
                    placesViewModel.selectedPlaceItem.value = placeItem

                    // instantiate the detail fragment from the factory
                    val surfSpotDetailFragment = childFragmentManager.fragmentFactory.instantiate(
                        SurfSpotDetailFragment::class.java.classLoader!!,
                        SurfSpotDetailFragment::class.java.name
                    )
                    surfSpotDetailFragment.sharedElementEnterTransition = IncomingTransition()

                    // push the fragment onto the stack
                    val fragmentManager = activity?.supportFragmentManager
                    fragmentManager?.let {
                        val transaction = it.beginTransaction().apply {

                            val transitionName = "imageTransition$position"
                            placesViewModel.currentTransition.postValue(transitionName)

                            surfSpotDetailFragment.sharedElementEnterTransition =
                                TransitionInflater
                                    .from(context)
                                    .inflateTransition(android.R.transition.move)

                            // add the view to the transition to link animation
                            viewManager?.findViewByPosition(position)?.let { view ->
                                val imageView = view.findViewById(R.id.image_place) as View
                                addSharedElement(imageView, transitionName)
                            }

                            replace(R.id.fragmentContainer, surfSpotDetailFragment)
                            addToBackStack(null)
                        }
                        transaction.commit()
                    }
                }
            }
        }

        // setup the RecyclerView
        binding.placesList.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }

    override fun onDestroyView() {
        binding.placesList.adapter = null
        viewAdapter = null
        viewManager = null

        super.onDestroyView()
    }
}
