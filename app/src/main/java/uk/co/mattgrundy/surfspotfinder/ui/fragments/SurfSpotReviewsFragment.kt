package uk.co.mattgrundy.surfspotfinder.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.app_bar_header.*
import kotlinx.android.synthetic.main.surf_spot_list_item.*
import uk.co.mattgrundy.surfspotfinder.R
import uk.co.mattgrundy.surfspotfinder.databinding.FragmentSurfSpotMapBinding
import uk.co.mattgrundy.surfspotfinder.databinding.SurfSpotReviewsBinding
import uk.co.mattgrundy.surfspotfinder.lib.data.viewmodel.PlacesViewModel
import uk.co.mattgrundy.surfspotfinder.ui.adapters.SurfSpotReviewsAdapter
import javax.inject.Inject

/**
 * Fragment to show a list of places
 */
class SurfSpotReviewsFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory
) : DaggerFragment() {

    private val placesViewModel: PlacesViewModel by activityViewModels {
        viewModelFactory
    }

    private var viewAdapter: SurfSpotReviewsAdapter? = null
    private var viewManager: RecyclerView.LayoutManager? = null

    private var _binding: SurfSpotReviewsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = SurfSpotReviewsBinding.inflate(inflater, container, false)
        return binding.root
    }

    private fun setupToolbar(toolbar: Toolbar) {
        if (activity is AppCompatActivity) {
            (activity as AppCompatActivity).run {
                setSupportActionBar(toolbar)
                supportActionBar?.setDisplayHomeAsUpEnabled(true)
                toolbar.setNavigationOnClickListener { onBackPressed() }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar(toolbar)

        // observe the places list
        placesViewModel.place.observe(viewLifecycleOwner, Observer {
            viewAdapter?.data = it.result.reviews
        })

        viewManager = LinearLayoutManager(activity)
        viewAdapter = SurfSpotReviewsAdapter()

        // setup the RecyclerView
        binding.placesList.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }

    override fun onDestroyView() {
        viewAdapter = null
        viewManager = null

        super.onDestroyView()
    }
}
