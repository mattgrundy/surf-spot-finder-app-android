package uk.co.mattgrundy.surfspotfinder.lib.dagger.modules

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import uk.co.mattgrundy.surfspotfinder.lib.repository.persistence.database.AppDatabase
import uk.co.mattgrundy.surfspotfinder.lib.repository.persistence.database.MIGRATION_1_2
import javax.inject.Singleton

/**
 * Repository module returns interfaces for each API endpoint
 */
@Module
class DatabaseRepository {

    /**
     * Provides app database instance
     * @param context App context
     * @return AppDatabase instance
     */
    @Provides
    @Singleton
    fun provideAppDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            "places_database"
        )
            .fallbackToDestructiveMigration()
            .build()
    }
}
