package uk.co.mattgrundy.surfspotfinder.lib.listeners

import android.content.Context
import android.location.Location
import android.os.Looper
import com.google.android.gms.location.*
import uk.co.mattgrundy.surfspotfinder.lib.config.CurrentEnvironment
import uk.co.mattgrundy.surfspotfinder.lib.data.models.location.LocationModel
import javax.inject.Inject

/**
 * Listener class for watching location changes
 * Can be used either:
 *  - by calling onStart/onStop and binding a location callback for continued updates
 *  - by calling getLastLocation to get the last known location
 */
class LocationListener @Inject constructor(context: Context) {
    private var fusedLocationClient: FusedLocationProviderClient =
        LocationServices.getFusedLocationProviderClient(context)

    private var locationCallback: LocationCallback? = null
    var onLocationUpdated: OnLocationUpdated? = null

    /**
     * Call to setup a location listener instance
     * typically onResume() in a fragment or activity context
     */
    fun onStart() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult?.let {
                    // use the most recent location provided
                    onLocationUpdated?.locationUpdated(
                        LocationModel(it.lastLocation.latitude, it.lastLocation.longitude)
                    )
                    super.onLocationResult(locationResult)
                }
            }
        }
    }

    /**
     * Call to stop a location callback
     * typically onPause() in a fragment or activity context
     */
    fun onStop() {
        if (locationCallback != null) {
            fusedLocationClient.removeLocationUpdates(
                locationCallback
            )
        }
    }

    /**
     * Get the last known location, will rarely return null
     */
    fun getLastLocation() {
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                if(location != null){
                    onLocationUpdated?.locationUpdated(
                        LocationModel(
                            location.latitude,
                            location.longitude
                        )
                    )
                } else {
                    onLocationUpdated?.locationUpdated(
                        LocationModel(
                            CurrentEnvironment.defaultLocation.latitude,
                            CurrentEnvironment.defaultLocation.longitude
                        )
                    )
                }
            }
    }

    /**
     * Bind a location callback to the location listener
     * with the specified update intervals
     * @param locationCallback Location callback to call when location updated
     * @param updateInterval Suggested interval between location updates
     * @param fastestUpdateInterval Minimum interval between location updates
     * @param maxWaitTime Maximum interval between location updates
     */
    fun bindLocationCallback(
        locationCallback: LocationCallback,
        updateInterval: Long = 6000,
        fastestUpdateInterval: Long = 3000,
        maxWaitTime: Long = updateInterval * 5
    ) {
        this.locationCallback = locationCallback

        val locationRequest = LocationRequest();
        locationRequest.interval = updateInterval;
        locationRequest.fastestInterval = fastestUpdateInterval;
        locationRequest.maxWaitTime = maxWaitTime;

        fusedLocationClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            Looper.getMainLooper()
        )
    }

    /**
     * Interface for location callbacks
     */
    interface OnLocationUpdated {
        fun locationUpdated(location: LocationModel)
    }
}