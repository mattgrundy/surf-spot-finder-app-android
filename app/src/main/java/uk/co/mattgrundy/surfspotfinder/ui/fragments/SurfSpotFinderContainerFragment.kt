package uk.co.mattgrundy.surfspotfinder.ui.fragments

import android.Manifest
import android.os.Bundle
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayoutMediator
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.app_bar_header_listing.*
import kotlinx.android.synthetic.main.surf_spot_finder_container_fragment.*
import timber.log.Timber
import uk.co.mattgrundy.surfspotfinder.R
import uk.co.mattgrundy.surfspotfinder.databinding.SurfSpotFinderContainerFragmentBinding
import uk.co.mattgrundy.surfspotfinder.lib.data.models.location.LocationModel
import uk.co.mattgrundy.surfspotfinder.lib.data.models.location.SearchParams
import uk.co.mattgrundy.surfspotfinder.lib.data.models.state.LoadingState
import uk.co.mattgrundy.surfspotfinder.lib.data.viewmodel.PlacesViewModel
import uk.co.mattgrundy.surfspotfinder.lib.listeners.LocationListener
import uk.co.mattgrundy.surfspotfinder.lib.listeners.PermissionListener
import uk.co.mattgrundy.surfspotfinder.ui.adapters.ViewTypePagerAdapter
import javax.inject.Inject

/**
 * Fragment to show a search bar and view pager hosting the listing and map fragments
 */
class SurfSpotFinderContainerFragment @Inject constructor(
    private val locationListener: LocationListener,
    private val permissionListener: PermissionListener,
    private val viewModelFactory: ViewModelProvider.Factory
) : DaggerFragment() {

    private val placesViewModel: PlacesViewModel by activityViewModels {
        viewModelFactory
    }

    // default sort order for search results
    private var searchOrder = SearchParams.SearchOrder.DISTANCE

    private var locationSnackbar: Snackbar? = null

    private var hasBeenGranted = false

    private var tabLayoutMediator: TabLayoutMediator? = null

    private var _binding: SurfSpotFinderContainerFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)

        _binding = SurfSpotFinderContainerFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    private fun setupToolbar(toolbar: Toolbar) {
        if (activity is AppCompatActivity) {
            (activity as AppCompatActivity).setSupportActionBar(toolbar)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar(toolbar)

        val dayAdapter = ViewTypePagerAdapter(this)
        binding.pager.adapter = dayAdapter
        binding.pager.isUserInputEnabled = false

        tabLayoutMediator = TabLayoutMediator(tab_layout, pager) { tab, position ->
            tab.text = dayAdapter.getTabTitle(position)
        }
        tabLayoutMediator?.attach()

        // watch for submit action on text field
        binding.appBarLayout.textSearch.setOnEditorActionListener { _, keyEventCode, _ ->
            when (keyEventCode) {
                EditorInfo.IME_ACTION_SEARCH -> {
                    // trigger the search
                    searchParamsUpdated()
                    // hide the keyboard
                    text_search.onEditorAction(EditorInfo.IME_ACTION_DONE)
                    true
                }
                else -> false
            }
        }

        // add a listener for location
        locationListener.onLocationUpdated = object : LocationListener.OnLocationUpdated {
            override fun locationUpdated(location: LocationModel) {
                placesViewModel.locationModel.postValue(location)
            }
        }

        // listen for permission changes
        permissionListener.observePermission(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            object : PermissionListener.OnPermissionChanged {
                override fun permissionChange(permissionType: String, granted: Boolean) {
                    if (!granted) {
                        showLocationUnavailableSnackBar()
                    } else if (permissionType == Manifest.permission.ACCESS_COARSE_LOCATION) {
                        if (!hasBeenGranted) {
                            hasBeenGranted = true
                            locationSnackbar?.dismiss()
                            locationListener.getLastLocation()
                        }
                    }
                }
            }
        )

        // watch for location changes
        placesViewModel.locationModel.observe(viewLifecycleOwner, Observer {
            doSearch()
        })

        // watch for search params changes
        placesViewModel.searchParams.observe(viewLifecycleOwner, Observer {
            doSearch()
        })

        // observe the loading state
        placesViewModel.loadingState.observe(viewLifecycleOwner, Observer { loadingState ->
            // show the loading view
            binding.progressView.visibility =
                if (loadingState.loadingStateType == LoadingState.LoadingStateType.LOADING) {
                    View.VISIBLE
                } else {
                    View.GONE
                }

            // handle any errors
            loadingState.message?.run {
                Toast.makeText(activity, this, Toast.LENGTH_LONG).show()
            }
        })

    }

    /**
     * Show a Snackbar asking for location
     */
    fun showLocationUnavailableSnackBar() {
        locationSnackbar = Snackbar.make(
            requireView(),
            getString(R.string.location_unavailable_text),
            Snackbar.LENGTH_INDEFINITE
        )
        locationSnackbar?.setAction(R.string.location_unavailable_button_allow) {
            permissionListener.requestPermission(
                this@SurfSpotFinderContainerFragment,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
        }
        locationSnackbar?.show()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_main, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_order -> {
                toggleOrder(item)
                searchParamsUpdated()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun toggleOrder(menuItem: MenuItem) {
        searchOrder = if (searchOrder == SearchParams.SearchOrder.DISTANCE) {
            menuItem.setIcon(R.drawable.ic_menu_sort_by_size)
            SearchParams.SearchOrder.RATING
        } else {
            menuItem.setIcon(R.drawable.road)
            SearchParams.SearchOrder.DISTANCE
        }

        val orderDisplay = getString(
            R.string.search_toast_order_by,
            if (searchOrder == SearchParams.SearchOrder.DISTANCE) {
                "distance"
            } else {
                "prominence"
            }
        )

        Toast.makeText(activity, orderDisplay, Toast.LENGTH_SHORT).show()
    }

    private fun searchParamsUpdated() {
        val searchText = text_search.text.toString()

        Timber.d("Doing search for $searchText")

        placesViewModel.searchParams.postValue(
            SearchParams(
                searchText,
                searchOrder
            )
        )
    }

    private fun doSearch() {
        placesViewModel.getPlacesNearby(
            placesViewModel.searchParams.value?.searchText,
            placesViewModel.searchParams.value?.searchOrder,
            placesViewModel.locationModel.value,
            placesViewModel.searchParams.value?.searchRadius
                ?: SearchParams.RADIUS_METERS_SEARCH_DISTANCE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        permissionListener.onRequestPermissionsResult(requestCode, permissions, grantResults)

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onDestroyView() {
        locationSnackbar = null
        permissionListener.onPermissionListener = null
        locationListener.onLocationUpdated = null
        pager.adapter = null
        tabLayoutMediator?.detach()

        super.onDestroyView()
    }
}