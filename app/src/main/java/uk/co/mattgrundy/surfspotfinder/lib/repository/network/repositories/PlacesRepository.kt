package uk.co.mattgrundy.surfspotfinder.lib.repository.network.repositories

import timber.log.Timber
import uk.co.mattgrundy.surfspotfinder.lib.data.models.location.SearchParams
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.PlaceContainer
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.PlaceResult
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.PlacesContainer
import uk.co.mattgrundy.surfspotfinder.lib.repository.network.api.PlacesApi
import uk.co.mattgrundy.surfspotfinder.lib.repository.persistence.converters.DbDataConverter
import uk.co.mattgrundy.surfspotfinder.lib.repository.persistence.database.AppDatabase
import java.util.*
import javax.inject.Inject

class PlacesRepository @Inject constructor(
    private val placesApi: PlacesApi,
    private val appDatabase: AppDatabase
) {
    companion object {
        const val REFRESH_DATA_TIME_MAX = 60 * 60 * 5 // 5 mins
    }

    var placesLastRefreshTime = 0L

    /**
     * Fetch a listing of places nearby
     * @param searchText Search text to match
     * @param latitude Latitude to search around
     * @param longitude Longitude to search around
     * @param radius Radius from lat,lng to search from
     * @return Live data instance containing PlacesContainer
     */
    suspend fun getPlacesNearby(
        searchText: String?,
        searchOrder: SearchParams.SearchOrder,
        latitude: Double,
        longitude: Double,
        radius: Int
    ): RepositoryResult<PlacesContainer> {

        /*
        // check last refresh time and return from local cache
        if ((Date().time - placesLastRefreshTime) < REFRESH_DATA_TIME_MAX) {
            Timber.d("Returning data from local cache")

            val placeEntities = appDatabase.placesDao().getAllPlaces()

            val placeContainers = ArrayList<PlaceResult>()
            for (placeEntity in placeEntities) {
                placeContainers.add(PlaceResult(placeEntity))
            }

            return RepositoryResult(
                true,
                null,
                PlacesContainer(placeContainers)
            )
        }

        Timber.d("Returning data from network")
        */

        // Google Places require location in the format lat,lng
        val locationFormatted = "$latitude,$longitude"

        // radius should be null if ordering by rating
        val radiusDistance = if (searchOrder == SearchParams.SearchOrder.DISTANCE) {
            null
        } else {
            radius
        }

        // type will always be 'lodging'
        val resultsType = "lodging"

        val fields = arrayOf(
            "photos", "formatted_address", "name", "rating", "opening_hours", "geometry"
        ).joinToString(",")

        val apiResponse = placesApi.getPlacesFromTextWithLocation(
            searchText,
            searchOrder.value,
            locationFormatted,
            resultsType,
            radiusDistance,
            fields
        )

        for (result in apiResponse.results) {
            result.hasFetchedDetails = false
        }

        /*
        appDatabase.placesDao().insertAll(
            DbDataConverter.convertPlaces(apiResponse.results).toTypedArray()
        )
        */

        placesLastRefreshTime = Date().time

        return RepositoryResult(
            true,
            null,
            apiResponse
        )
    }

    /**
     * Fetch a place details
     * @param placeId The ID of the place to find
     * @return Live data instance containing PlaceContainer
     */
    suspend fun getPlaceById(
        placeId: String
    ): RepositoryResult<PlaceContainer> {

        // check last refresh time and return from local cache
        val maxAge = Date().time - REFRESH_DATA_TIME_MAX

        Timber.d("Returning place ($placeId) from local cache")

        val place = appDatabase.placesDao().getPlace(placeId, maxAge)

        place?.let {
            return RepositoryResult(
                true,
                null,
                PlaceContainer(PlaceResult(place))
            )
        }

        Timber.d("Returning no record for place")

        Timber.d("Returning place from network")

        val fields = arrayOf(
            "id", "place_id", "icon", "photos", "formatted_address", "name",
            "formatted_phone_number", "rating", "opening_hours", "geometry", "review",
            "user_ratings_total"
        ).joinToString(",")

        val apiResponse = placesApi.getPlaceDetails(
            placeId,
            fields
        )

        placesLastRefreshTime = Date().time

        apiResponse.result.hasFetchedDetails = true
        apiResponse.result.lastRefreshTime = Date().time
        appDatabase.placesDao().insertPlace(
            DbDataConverter.convertPlace(apiResponse.result)
        )

        return RepositoryResult(
            true,
            null,
            apiResponse
        )
    }

    class RepositoryResult<T>(
        val success: Boolean,
        val message: String?,
        val body: T?
    )

}