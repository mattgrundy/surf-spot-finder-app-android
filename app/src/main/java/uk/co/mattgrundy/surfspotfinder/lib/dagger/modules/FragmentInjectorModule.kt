package uk.co.mattgrundy.surfspotfinder.lib.dagger.modules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import uk.co.mattgrundy.surfspotfinder.MainActivity
import uk.co.mattgrundy.surfspotfinder.ui.fragments.*

/**
 * Generates injectors for fragments
 */
@Module
abstract class FragmentInjectorModule {
    @ContributesAndroidInjector
    abstract fun contributeSurfSpotFinderContainerFragmentInjector(): SurfSpotFinderContainerFragment

    @ContributesAndroidInjector
    abstract fun contributeSurfSpotListItemFragmentInjector(): SurfSpotListItemFragment

    @ContributesAndroidInjector
    abstract fun contributeSurfSpotMapItemFragmentInjector(): SurfSpotMapFragment

    @ContributesAndroidInjector
    abstract fun contributeSurfSpotDetailFragmentInjector(): SurfSpotDetailFragment

    @ContributesAndroidInjector
    abstract fun contributeSurfSpotReviewsFragmentInjector(): SurfSpotReviewsFragment

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity
}
