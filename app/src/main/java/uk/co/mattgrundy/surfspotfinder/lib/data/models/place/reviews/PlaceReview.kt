package uk.co.mattgrundy.surfspotfinder.lib.data.models.place.reviews

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "place_review")
data class PlaceReview(
    @PrimaryKey(autoGenerate = true)
    val id: Int?,

    val author_name: String,
    val rating: Int,
    val relative_time_description: String,
    val text: String
) {
    var placeId: String = ""
}