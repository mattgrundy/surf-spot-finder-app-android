package uk.co.mattgrundy.surfspotfinder.lib.dagger.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import uk.co.mattgrundy.surfspotfinder.lib.listeners.LocationListener
import uk.co.mattgrundy.surfspotfinder.lib.listeners.PermissionListener
import javax.inject.Singleton

/**
 * Provide listeners used in the app
 */
@Module
class ListenerModule {

    /**
     * Provides a location listener instance for listening to location changes
     * @param context current app context
     * @return LocationListener instance
     */
    @Provides
    @Singleton
    fun provideLocationsListener(context: Context): LocationListener {
        return LocationListener(context)
    }

    /**
     * Provides a permission listener instance for listening to permission changes
     * @return PermissionListener instance
     */
    @Provides
    @Singleton
    fun providePermissionListener(): PermissionListener {
        return PermissionListener()
    }
}
