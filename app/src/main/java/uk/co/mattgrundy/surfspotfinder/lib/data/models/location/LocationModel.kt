package uk.co.mattgrundy.surfspotfinder.lib.data.models.location

data class LocationModel(
    val latitude: Double,
    val longitude: Double
)