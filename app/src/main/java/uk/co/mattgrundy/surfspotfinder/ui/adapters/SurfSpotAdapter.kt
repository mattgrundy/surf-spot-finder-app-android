package uk.co.mattgrundy.surfspotfinder.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import timber.log.Timber
import uk.co.mattgrundy.surfspotfinder.R
import uk.co.mattgrundy.surfspotfinder.lib.config.CurrentEnvironment
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.PlaceResult
import uk.co.mattgrundy.surfspotfinder.lib.listeners.OnListItemClickListener

/**
 * Adapter for displaying list of locations
 */
class SurfSpotAdapter(
    private val fragment: Fragment,
    private val currentEnvironment: CurrentEnvironment.Environment
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val VIEW_TYPE_ITEM = 0
        const val VIEW_TYPE_NO_RESULTS = 1
    }

    // interface for item click callbacks
    var onListItemClickListener: OnListItemClickListener? = null

    // list of data to display
    var data: List<PlaceResult>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    class PlaceItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textTitle: TextView = view.findViewById(R.id.text_title)
        val imagePlace: ImageView = view.findViewById(R.id.image_place)
        val rating: RatingBar = view.findViewById(R.id.rating)
        val noRatings: TextView = view.findViewById(R.id.no_ratings)
        val textOpenNow: TextView = view.findViewById(R.id.text_open_now)
    }

    class NoResultsViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun getItemViewType(position: Int): Int {
        return when (data?.size) {
            0 -> VIEW_TYPE_NO_RESULTS
            else -> VIEW_TYPE_ITEM
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return when (data?.size) {
            0 -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_no_results, parent, false)
                NoResultsViewHolder(view)
            }
            else -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_place, parent, false)
                PlaceItemViewHolder(view)
            }

        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is PlaceItemViewHolder -> {
                val rowData = data?.get(position);

                holder.imagePlace.transitionName = "imageTransition$position";

                // bind the on click listener for row clicks
                holder.itemView.setOnClickListener {
                    onListItemClickListener?.onItemClick(position)
                }

                holder.textTitle.text = rowData?.name
                holder.rating.rating = rowData?.rating?.toFloat() ?: 0f
                holder.noRatings.text =
                    fragment.getString(R.string.place_ratings, rowData?.user_ratings_total ?: 0)

                // show a message if it's currently open
                val openNowText = when (rowData?.opening_hours?.open_now) {
                    true -> fragment.getString(R.string.open_status_open)
                    false -> fragment.getString(R.string.open_status_closed)
                    else -> fragment.getString(R.string.open_status_unknown)
                }
                holder.textOpenNow.text = openNowText

                // reset the view holder image
                holder.imagePlace.setImageResource(R.drawable.building_silhouette)

                // load the image from the icon URL
                rowData?.getFirstPhoto()?.let { firstPhoto ->
                    val imageUrl = currentEnvironment.getImageUrl(firstPhoto.photo_reference)

                    Timber.d("Loading image: $imageUrl")

                    // bind Glide to the fragment so it's garbage collected when the fragment is destroyed
                    Glide.with(fragment)
                        .load(imageUrl)
                        .placeholder(R.drawable.building_silhouette)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(holder.imagePlace)
                }
            }
        }
    }

    override fun getItemCount() = when (data?.size) {
        // no data loaded yet
        null -> 0
        // data loaded but empty, show no results cell
        0 -> 1
        // data loaded, show the number of items
        else -> data!!.size
    }
}