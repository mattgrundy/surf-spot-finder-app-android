package uk.co.mattgrundy.surfspotfinder.lib.repository.network.api

import retrofit2.http.GET
import retrofit2.http.Query
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.PlaceContainer
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.PlacesContainer

/**
 * API interface for loading places
 */
interface PlacesApi {

    /**
     * Find places nearby a given location
     * @param search Keyword to search for
     * @param orderBy Order of results - typically `distance` or `prominence`
     * @param location Lat lng of center point, in the format {lat},{lng}
     * @param type Category of results to return
     * @param fields Fields to return with the results, comma separated list
     * @return Retrofit call instance referencing the network request
     */
    @GET("place/nearbysearch/json")
    suspend fun getPlacesFromTextWithLocation(
        @Query("keyword") search: String?,
        @Query("rankby") orderBy: String,
        @Query("location") location: String,
        @Query("type") type: String?,
        @Query("radius") radius: Int?,
        @Query("fields") fields: String
    ): PlacesContainer

    /**
     * Get the details of a place
     * @param placeId PlaceID of the place
     * @return Retrofit call instance referencing the network request
     */
    @GET("place/details/json")
    suspend fun getPlaceDetails(
        @Query("place_id") placeId: String,
        @Query("fields") fields: String
    ): PlaceContainer
}