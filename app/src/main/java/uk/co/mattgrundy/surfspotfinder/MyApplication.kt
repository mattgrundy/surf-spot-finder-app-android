package uk.co.mattgrundy.surfspotfinder

import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import timber.log.Timber
import timber.log.Timber.DebugTree
import uk.co.mattgrundy.surfspotfinder.lib.config.CurrentEnvironment
import uk.co.mattgrundy.surfspotfinder.lib.dagger.components.AppComponent
import uk.co.mattgrundy.surfspotfinder.lib.dagger.components.DaggerAppComponent
import javax.inject.Inject

class MyApplication : Application(), HasAndroidInjector {

    var appComponent: AppComponent? = null

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()

        if (CurrentEnvironment.getEnvironment().isTestEnvironment) {
            Timber.plant(DebugTree())
        }

        appComponent = initDagger(this)
        appComponent?.inject(this)
    }

    private fun initDagger(app: Application): AppComponent =
        DaggerAppComponent.builder()
            .application(app)
            .build()

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

}