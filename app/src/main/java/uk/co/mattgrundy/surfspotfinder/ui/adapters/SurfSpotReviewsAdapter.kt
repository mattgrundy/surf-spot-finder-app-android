package uk.co.mattgrundy.surfspotfinder.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import uk.co.mattgrundy.surfspotfinder.R
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.reviews.PlaceReview

/**
 * Adapter for displaying list of locations
 */
class SurfSpotReviewsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    // list of data to display
    var data: List<PlaceReview>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    class PlaceReviewViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textTitle: TextView = view.findViewById(R.id.text_title)
        val textDescription: TextView = view.findViewById(R.id.text_title)
        val rating: RatingBar = view.findViewById(R.id.rating)
    }

    override fun getItemViewType(position: Int): Int {
        return 0
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_review, parent, false)
        return PlaceReviewViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is PlaceReviewViewHolder) {
            val rowData = data?.get(position);

            holder.textTitle.text = rowData?.author_name
            holder.textDescription.text = rowData?.text
            holder.rating.rating = rowData?.rating?.toFloat() ?: 0f
        }
    }

    override fun getItemCount() = data?.size ?: 0
}