package uk.co.mattgrundy.surfspotfinder.lib.dagger.modules

import dagger.Module
import dagger.Provides
import uk.co.mattgrundy.surfspotfinder.lib.repository.network.api.PlacesApi
import uk.co.mattgrundy.surfspotfinder.lib.repository.network.repositories.PlacesRepository
import uk.co.mattgrundy.surfspotfinder.lib.repository.persistence.database.AppDatabase
import javax.inject.Singleton

/**
 * Repository module returns interfaces for each API endpoint
 */
@Module
class RepositoryModule {

    /**
     * Provides places repository instance
     * @param placesApi API interface to build the repository for
     * @return PlacesRepository instance
     */
    @Provides
    @Singleton
    fun providePlacesRepository(
        placesApi: PlacesApi,
        appDatabase: AppDatabase
    ): PlacesRepository {
        return PlacesRepository(placesApi, appDatabase)
    }
}
