package uk.co.mattgrundy.surfspotfinder.lib.dagger.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.Reusable
import uk.co.mattgrundy.surfspotfinder.lib.config.CurrentEnvironment

@Module
class EnvironmentModule {
    /**
     * Provides the Environment object.
     * @param context the current app context
     * @return the Environment object
     */
    @Provides
    @Reusable
    fun provideEnvironment(context: Context): CurrentEnvironment.Environment {
        val environment = CurrentEnvironment.getEnvironment()
        environment.initEnvironmentFromContext(context)

        return environment
    }

}