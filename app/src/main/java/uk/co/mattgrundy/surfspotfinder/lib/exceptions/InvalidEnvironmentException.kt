package uk.co.mattgrundy.surfspotfinder.lib.exceptions

/**
 * Exception for when an unknown environment type is requested
 */
class InvalidEnvironmentException(message: String?) :
    RuntimeException(message)
