package uk.co.mattgrundy.surfspotfinder.lib.dagger.modules

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Provide application context for dependent modules
 */
@Module
class AppModule {

    /**
     * Get the application context
     * @param application current application instance
     * @return application context
     */
    @Provides
    @Singleton
    fun provideContext(application: Application): Context {
        return application
    }
}