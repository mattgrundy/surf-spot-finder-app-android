package uk.co.mattgrundy.surfspotfinder.lib.data.models.state

data class LoadingState(
    val loadingStateType: LoadingStateType,
    val message: String? = null
) {
    enum class LoadingStateType {
        LOADING, LOADED, ERROR
    }
}