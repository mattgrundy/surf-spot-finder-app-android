package uk.co.mattgrundy.surfspotfinder.lib.repository.persistence.converters

import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.PlaceResult

class DbDataConverter {

    companion object {
        fun convertPlace(place: PlaceResult): PlaceResult {
            place.photos?.let {
                for (photo in it) {
                    photo.placeId = place.id
                }
            }

            place.reviews?.let {
                for (review in it) {
                    review.placeId = place.id
                }
            }

            return place
        }
    }

}