package uk.co.mattgrundy.surfspotfinder.lib.data.models.place

data class PlaceContainer(
    val result: PlaceResult
)