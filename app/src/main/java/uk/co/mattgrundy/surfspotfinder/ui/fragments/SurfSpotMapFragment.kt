package uk.co.mattgrundy.surfspotfinder.ui.fragments

import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import dagger.android.support.DaggerFragment
import timber.log.Timber
import uk.co.mattgrundy.surfspotfinder.R
import uk.co.mattgrundy.surfspotfinder.databinding.FragmentSurfSpotMapBinding
import uk.co.mattgrundy.surfspotfinder.databinding.SurfSpotListItemBinding
import uk.co.mattgrundy.surfspotfinder.lib.data.models.location.LocationModel
import uk.co.mattgrundy.surfspotfinder.lib.data.models.location.SearchParams
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.PlaceResult
import uk.co.mattgrundy.surfspotfinder.lib.data.viewmodel.PlacesViewModel
import javax.inject.Inject
import kotlin.math.max
import kotlin.math.roundToInt

/**
 * Fragment to show a map of places
 */
class SurfSpotMapFragment @Inject constructor(
    val viewModelFactory: ViewModelProvider.Factory
) : DaggerFragment(), OnMapReadyCallback {

    companion object {
        // minimum distance in map projection movement before refreshing the map
        // this is used as a factor of map zoom to make it more responsive at higher zoom levels
        const val MIN_MAP_MOVEMENT_REFRESH_DISTANCE = 7000

        // default zoom level for the map on first load
        const val MAP_INITIAL_ZOOM_LEVEL = 14f
    }

    // flag to stop re-centering the map on places list before location received
    private var skipSetCenter = false

    // flag to stop map refresh when setting initial location
    private var hasSetInitialLocation = false

    // this will be null until MapReady callback
    var mapView: GoogleMap? = null

    private var _binding: FragmentSurfSpotMapBinding? = null
    private val binding get() = _binding!!

    private val placesViewModel: PlacesViewModel by activityViewModels {
        viewModelFactory
    }

    // reference to the last center location projected
    private var lastLocation: Location? = null

    // map fragment instance
    private var mapFragment: SupportMapFragment? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSurfSpotMapBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mapFragment = childFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment
        mapFragment?.getMapAsync(this)

        // observe live data for our places results
        placesViewModel.places.observe(viewLifecycleOwner, Observer { places ->
            addMapItems(places.results)

            // recenter the map for the results
            if (!skipSetCenter) {
                setMapCenter()
            }
        })
    }

    private fun setMapCenter() {
        // see if we've got a current location in our view model
        placesViewModel.locationModel.value?.let {
            hasSetInitialLocation = true

            // set the map center and zoom
            mapView?.moveCamera(
                CameraUpdateFactory.newLatLngZoom(
                    LatLng(it.latitude, it.longitude),
                    MAP_INITIAL_ZOOM_LEVEL
                )
            )
        }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        Timber.d("Map loaded successfully")
        mapView = googleMap

        setMapCenter()

        // add a listener for map movements
        mapView?.let { map ->
            map.setOnCameraIdleListener {
                if (hasSetInitialLocation) {
                    skipSetCenter = true

                    val newPosition = map.cameraPosition.target

                    val newLocation = Location("")
                    newLocation.latitude = newPosition.latitude
                    newLocation.longitude = newPosition.longitude

                    locationChanged(newLocation)
                }
            }
        }
    }

    private fun locationChanged(newLocation: Location) {

        mapView?.let { map ->
            // distance between last location and new location
            val distanceMoved = lastLocation?.distanceTo(newLocation) ?: 0f

            // movement required to update map
            // a factor of zoom level to make it more responsive at higher zoom levels
            val mapScale = map.cameraPosition.zoom / 2f
            val requiredMovement = (MIN_MAP_MOVEMENT_REFRESH_DISTANCE / mapScale)

            Timber.d(
                "Amount Map Moved: %f Amount Required: %f (%d / %f)",
                distanceMoved,
                requiredMovement,
                MIN_MAP_MOVEMENT_REFRESH_DISTANCE,
                mapScale
            )

            if (lastLocation == null || distanceMoved > requiredMovement) {

                // find the bounds of the map for querying places within
                val mapBounds = map.projection.visibleRegion.latLngBounds

                val location = FloatArray(2)
                Location.distanceBetween(
                    mapBounds.northeast.latitude,
                    mapBounds.northeast.longitude,
                    mapBounds.southwest.latitude,
                    mapBounds.southwest.longitude,
                    location
                )

                // update the search params
                placesViewModel.searchParams.value?.searchOrder = SearchParams.SearchOrder.DISTANCE
                placesViewModel.searchParams.value?.searchRadius =
                    max(location[0], location[0]).roundToInt()

                // pos the new center map location as our location to trigger a new search
                placesViewModel.locationModel.postValue(
                    LocationModel(newLocation.latitude, newLocation.longitude)
                )

                // store the center of the map for checking map movement
                lastLocation = newLocation
            }
        }
    }

    /**
     * Add place markers to the maps
     * @param places list of PlaceResult to display on the map
     */
    private fun addMapItems(places: List<PlaceResult>) {
        mapView?.let { mapInstance ->
            // clear current markers
            mapInstance.clear()

            // add the markers to the map
            for (place in places) {
                Timber.d("Adding map marker: %s", place.geometry.location)

                mapInstance.addMarker(
                    MarkerOptions()
                        .title(place.name)
                        .position(LatLng(place.geometry.location.lat, place.geometry.location.lng))
                )
            }
        }
    }

    override fun onDestroyView() {
        lastLocation = null

        // deallocate the MapView instance
        mapView?.clear()
        mapView = null

        // tell the child fragment to destroy
        mapFragment?.onDestroyView()
        mapFragment = null

        super.onDestroyView()
    }
}
