package uk.co.mattgrundy.surfspotfinder.lib.dagger.modules

import androidx.fragment.app.Fragment
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import uk.co.mattgrundy.surfspotfinder.ui.fragments.*
import kotlin.reflect.KClass

/**
 * Fragment factory module for binding fragment instances
 * Binds using Fragment class name using Dagger multibinding
 */
@Module
abstract class FragmentFactoryModule {

    @Binds
    abstract fun bindFragmentFactory(factory: uk.co.mattgrundy.surfspotfinder.lib.dagger.factory.FragmentFactory): androidx.fragment.app.FragmentFactory

    @Binds
    @IntoMap
    @FragmentKey(SurfSpotFinderContainerFragment::class)
    abstract fun bindSurfSpotFinderContainerFragment(fragment: SurfSpotFinderContainerFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SurfSpotListItemFragment::class)
    abstract fun bindSurfSpotListItemFragment(fragment: SurfSpotListItemFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SurfSpotMapFragment::class)
    abstract fun bindSurfSpotMapFragment(fragment: SurfSpotMapFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SurfSpotDetailFragment::class)
    abstract fun bindSurfSpotDetailFragment(fragment: SurfSpotDetailFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SurfSpotReviewsFragment::class)
    abstract fun bindSurfSpotReviewsFragment(fragment: SurfSpotReviewsFragment): Fragment
}

@MapKey
annotation class FragmentKey(val clazz: KClass<out Fragment>)