package uk.co.mattgrundy.surfspotfinder.lib.data.models.place

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import uk.co.mattgrundy.surfspotfinder.lib.data.models.entities.PlaceEntity
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.reviews.PlaceReview

@Entity(tableName = "place_result")
data class PlaceResult(
    @PrimaryKey
    var id: String = "",

    var place_id: String = "",
    var name: String = "",
    var icon: String = "",

    @Embedded
    var geometry: PlaceLocationContainer = PlaceLocationContainer(PlaceLocation()),

    @Ignore
    var photos: List<PlacePhoto>? = null,

    var rating: Double? = null,
    var user_ratings_total: Int? = null,
    var formatted_address: String? = null,
    var formatted_phone_number: String? = null,

    @Ignore
    var reviews: List<PlaceReview>? = null
) {

    var hasFetchedDetails = false
    var lastRefreshTime = 0L

    constructor(
        placeEntity: PlaceEntity
    ) : this() {
        placeEntity.placeResult?.let {
            id = it.id
            place_id = it.place_id
            name = it.name
            icon = it.icon
            geometry = it.geometry
            rating = it.rating
            user_ratings_total = it.user_ratings_total
            formatted_address = it.formatted_address
            formatted_phone_number = it.formatted_phone_number
            photos = placeEntity.photos
            reviews = placeEntity.reviews
        }
    }

    @Embedded
    var opening_hours: PlaceOpeningHours? = null

    override fun toString(): String {
        return name
    }

    fun getFirstPhoto(): PlacePhoto? {
        return photos?.first()
    }
}