package uk.co.mattgrundy.surfspotfinder.lib.data.models.location

data class SearchParams(
    var searchText: String,
    var searchOrder: SearchOrder,
    var searchRadius: Int = RADIUS_METERS_SEARCH_DISTANCE
) {
    companion object {
        // Radius from center lat lng to search in meters - default 1km
        const val RADIUS_METERS_SEARCH_DISTANCE = 1000
    }

    enum class SearchOrder(val value: String) {
        DISTANCE("distance"),
        RATING("prominence")
    }
}