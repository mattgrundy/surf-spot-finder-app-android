package uk.co.mattgrundy.surfspotfinder.lib.data.models.place

data class PlacesContainer(
    val results: List<PlaceResult>
)