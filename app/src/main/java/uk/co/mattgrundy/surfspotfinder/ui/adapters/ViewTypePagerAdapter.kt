package uk.co.mattgrundy.surfspotfinder.ui.adapters

import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import uk.co.mattgrundy.surfspotfinder.R
import uk.co.mattgrundy.surfspotfinder.ui.fragments.SurfSpotListItemFragment
import uk.co.mattgrundy.surfspotfinder.ui.fragments.SurfSpotMapFragment

/**
 * Adapter for ViewPager with 2 tabs; List, Map
 * @param fragment Container fragment
 */
class ViewTypePagerAdapter(
    val fragment: Fragment
) : FragmentStateAdapter(fragment) {

    /**
     * Number of items in the pager
     * @return number of items - static 2
     */
    override fun getItemCount(): Int = 2

    /**
     * Fragment instance for adapter position
     * @param position Adapter index
     * @return Fragment instance
     */
    override fun createFragment(position: Int): Fragment = when (position) {
        0 -> instantiateFragment(SurfSpotListItemFragment::class.java)
        else -> instantiateFragment(SurfSpotMapFragment::class.java)
    }

    /**
     * Return the title for the tab strip
     * @param position Tab index
     */
    fun getTabTitle(position: Int): String = when (position) {
        0 -> fragment.getString(R.string.view_pager_title_list)
        else -> fragment.getString(R.string.view_pager_title_map)
    }

    /**
     * Create instance of Fragment using FragmentFactory to inject our constructor dependencies
     * @param clazz Class to instantiate
     * @return Fragment instance
     */
    private fun instantiateFragment(clazz: Class<out Fragment>): Fragment {
        return fragment.childFragmentManager.fragmentFactory.instantiate(
            clazz.classLoader!!,
            clazz.name
        )
    }

}