package uk.co.mattgrundy.surfspotfinder.lib.data.models.place

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "place_location_container")
data class PlaceLocationContainer(
    @Embedded
    val location: PlaceLocation
)