package uk.co.mattgrundy.surfspotfinder.lib.data.models.place

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "place_photo")
data class PlacePhoto(
    @PrimaryKey(autoGenerate = true)
    val id: Int?,

    var placeId : String = "",

    val photo_reference: String = "",
    val height: Int = 0,
    val width: Int = 0
)