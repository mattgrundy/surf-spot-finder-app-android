package uk.co.mattgrundy.surfspotfinder.lib.data.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import uk.co.mattgrundy.surfspotfinder.lib.config.CurrentEnvironment
import uk.co.mattgrundy.surfspotfinder.lib.data.models.location.LocationModel
import uk.co.mattgrundy.surfspotfinder.lib.data.models.location.SearchParams
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.PlaceContainer
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.PlaceResult
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.PlacesContainer
import uk.co.mattgrundy.surfspotfinder.lib.data.models.state.LoadingState
import uk.co.mattgrundy.surfspotfinder.lib.repository.network.repositories.PlacesRepository
import java.io.IOException
import javax.inject.Inject

/**
 * ViewModel for places. LiveData instances for posting and observing
 * changes to search params, location and updated search results
 */
class PlacesViewModel @Inject constructor(
    private val placesRepository: PlacesRepository
) : ViewModel() {

    // list of places updates from repository
    var places: MutableLiveData<PlacesContainer> = MutableLiveData()
    // place details updates from repository
    var place: MutableLiveData<PlaceContainer> = MutableLiveData()

    // current location
    val locationModel = MutableLiveData<LocationModel>()
    // current app state
    var loadingState = MutableLiveData<LoadingState>()
    // search parameters - search text + order
    var searchParams = MutableLiveData<SearchParams>()

    // current transition name
    var currentTransition = MutableLiveData<String>()

    // selected place
    val selectedPlaceItem = MutableLiveData<PlaceResult>()

    /**
     * Make request to the repository to fetch nearby places, results posted to places instance
     * @param searchText Text to search for
     * @param searchOrder Order of results
     * @param location Location to search from
     * @param radius Distance in meters from location to search
     */
    fun getPlacesNearby(
        searchText: String?,
        searchOrder: SearchParams.SearchOrder?,
        location: LocationModel?,
        radius: Int
    ) {
        loadingState.postValue(LoadingState(LoadingState.LoadingStateType.LOADING))

        val searchLocation = location ?: CurrentEnvironment.defaultLocation

        viewModelScope.launch(Dispatchers.IO) {
            try {
                val repositoryResponse = placesRepository.getPlacesNearby(
                    searchText,
                    searchOrder ?: SearchParams.SearchOrder.DISTANCE,
                    searchLocation.latitude,
                    searchLocation.longitude,
                    radius
                )

                places.postValue(repositoryResponse.body)
                loadingState.postValue(LoadingState(LoadingState.LoadingStateType.LOADED))
            } catch (e: IOException) {
                loadingState.postValue(
                    LoadingState(LoadingState.LoadingStateType.ERROR, e.localizedMessage)
                )
            }
        }
    }

    /**
     * Get the details of a place, results posted to place instance
     * @param placeId Google ID of the place
     */
    fun getPlaceDetails(placeId: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val repositoryResponse = placesRepository.getPlaceById(placeId)

            try {
                place.postValue(repositoryResponse.body)
                loadingState.postValue(LoadingState(LoadingState.LoadingStateType.LOADED))
            } catch (e: IOException) {
                loadingState.postValue(
                    LoadingState(LoadingState.LoadingStateType.ERROR, e.localizedMessage)
                )
            }
        }
    }
}