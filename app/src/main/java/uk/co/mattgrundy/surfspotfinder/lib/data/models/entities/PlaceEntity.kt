package uk.co.mattgrundy.surfspotfinder.lib.data.models.entities

import androidx.room.Embedded
import androidx.room.Relation
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.PlacePhoto
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.PlaceResult
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.reviews.PlaceReview

class PlaceEntity {

    @Embedded
    var placeResult : PlaceResult? = null

    @Relation(
        parentColumn = "id",
        entityColumn = "placeId"
    )
    var photos: List<PlacePhoto>? = null

    @Relation(
        parentColumn = "id",
        entityColumn = "placeId"
    )
    var reviews: List<PlaceReview>? = null

}