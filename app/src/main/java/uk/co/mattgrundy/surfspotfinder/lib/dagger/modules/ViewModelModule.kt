package uk.co.mattgrundy.surfspotfinder.lib.dagger.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import uk.co.mattgrundy.surfspotfinder.lib.dagger.factory.ViewModelFactory
import uk.co.mattgrundy.surfspotfinder.lib.data.viewmodel.PlacesViewModel
import kotlin.reflect.KClass

/**
 * ViewModel factory module for binding ViewModel instances
 * Binds using ViewModel class name using Dagger multibinding
 */
@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(PlacesViewModel::class)
    abstract fun bindPlacesViewModel(placesViewModel: PlacesViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory
}

@Retention(AnnotationRetention.RUNTIME)
@MapKey
annotation class ViewModelKey(val value: KClass<out ViewModel>)