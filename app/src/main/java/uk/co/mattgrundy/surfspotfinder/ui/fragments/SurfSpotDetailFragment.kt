package uk.co.mattgrundy.surfspotfinder.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.app_bar_header.*
import uk.co.mattgrundy.surfspotfinder.R
import uk.co.mattgrundy.surfspotfinder.databinding.RowDetailImageBinding
import uk.co.mattgrundy.surfspotfinder.databinding.SurfSpotDetailBinding
import uk.co.mattgrundy.surfspotfinder.lib.config.CurrentEnvironment
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.PlaceResult
import uk.co.mattgrundy.surfspotfinder.lib.data.viewmodel.PlacesViewModel
import javax.inject.Inject

/**
 * Fragment for display the details of a chosen place
 */
class SurfSpotDetailFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    protected val currentEnvironment: CurrentEnvironment.Environment
) : DaggerFragment() {

    private var _binding: SurfSpotDetailBinding? = null
    private val binding get() = _binding!!

    private var imageAdapter: ImageViewPagerAdapter? = null

    private val placesViewModel: PlacesViewModel by activityViewModels {
        viewModelFactory
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = SurfSpotDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar(toolbar)

        // observe selected place item
        placesViewModel.selectedPlaceItem.observe(viewLifecycleOwner, Observer {
            placesViewModel.getPlaceDetails(it.place_id)
            updateUi(it, withImage = true)
        })

        // observe for place details
        placesViewModel.place.observe(viewLifecycleOwner, Observer { place ->
            updateUi(place.result, withImage = false)
        })
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.clear()
    }

    private fun setupToolbar(toolbar: Toolbar) {
        if (activity is AppCompatActivity) {
            (activity as AppCompatActivity).run {
                setSupportActionBar(toolbar)
                supportActionBar?.setDisplayHomeAsUpEnabled(true)
                toolbar.setNavigationOnClickListener { onBackPressed() }
            }
        }
    }

    private fun updateUi(placeResult: PlaceResult, withImage: Boolean) {
        binding.textTitle.text = placeResult.name
        binding.rating.rating = placeResult.rating?.toFloat() ?: 0f
        binding.noRatings.text =
            getString(R.string.place_ratings, placeResult.user_ratings_total ?: 0)

        formatDisplayText(binding.textAddress, placeResult.formatted_address)
        formatDisplayText(binding.textTelephone, placeResult.formatted_phone_number)

        // set the opening hours, or hide the column if not available
        val weekdayText = placeResult.opening_hours?.weekday_text
        if (weekdayText?.isNotEmpty() == true) {
            binding.textOpeningHours.visibility = View.VISIBLE
            binding.textOpeningHours.text = weekdayText.joinToString("\n")
        } else {
            binding.textOpeningHours.visibility = View.GONE
        }

        if (placeResult.photos != null) {
            val photoUrls = placeResult.photos!!.map {
                currentEnvironment.getImageUrl(it.photo_reference)
            }

            imageAdapter = ImageViewPagerAdapter(this, photoUrls)
            binding.imagePager.adapter = imageAdapter
        } else {
            binding.imagePager.visibility = View.GONE
        }

        if (placeResult.reviews != null) {
            binding.ratingContainer.setOnClickListener {
                // instantiate the detail fragment from the factory
                val surfSpotReviewsFragment = childFragmentManager.fragmentFactory.instantiate(
                    SurfSpotReviewsFragment::class.java.classLoader!!,
                    SurfSpotReviewsFragment::class.java.name
                )

                // push the fragment onto the stack
                val fragmentManager = activity?.supportFragmentManager
                fragmentManager?.let {
                    it.beginTransaction().apply {
                        replace(R.id.fragmentContainer, surfSpotReviewsFragment)
                        addToBackStack(null)
                    }.commit()
                }
            }
        }
    }

    private fun formatDisplayText(textViewField: TextView, value: String?) {
        if (value?.isNotEmpty() == true) {
            textViewField.visibility = View.VISIBLE
            textViewField.text = value
        } else {
            textViewField.visibility = View.GONE
        }
    }

    override fun onDestroyView() {
        binding.imagePager.adapter = null
        imageAdapter = null

        super.onDestroyView()
    }

    /**
     * Adapter for Image pager
     * @param fragment Container fragment
     */
    class ImageViewPagerAdapter(
        val fragment: Fragment,
        private val photos: List<String>
    ) : FragmentStateAdapter(fragment) {

        /**
         * Number of items in the pager
         * @return number of photos to show
         */
        override fun getItemCount(): Int = photos.size

        /**
         * Fragment instance for adapter position
         * @param position Adapter index
         * @return Fragment instance
         */
        override fun createFragment(position: Int): Fragment {
            val surfSpotImageFragment = SurfSpotImageFragment()

            val bundle = Bundle()
            bundle.putString(
                SurfSpotImageFragment.ARG_IMAGE_NAME,
                photos.get(position)
            )

            surfSpotImageFragment.arguments = bundle

            return surfSpotImageFragment
        }
    }

    /**
     * Fragment for handling image pager view
     */
    class SurfSpotImageFragment : Fragment() {

        private var _binding: RowDetailImageBinding? = null
        private val binding get() = _binding!!

        private var imageName: String? = null

        companion object {
            const val ARG_IMAGE_NAME = "arg_image_name"
        }

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)

            imageName = arguments?.getString(ARG_IMAGE_NAME)
        }

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            _binding = RowDetailImageBinding.inflate(inflater, container, false)
            return binding.root
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)

            Glide.with(this)
                .load(imageName)
                .placeholder(R.drawable.building_silhouette)
                .into(binding.imagePlace)
                .apply {
                    RequestOptions().dontTransform()
                }
        }
    }

}
