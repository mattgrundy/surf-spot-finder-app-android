package uk.co.mattgrundy.surfspotfinder.lib.repository.persistence.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.PlacePhoto
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.PlaceResult
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.reviews.PlaceReview
import uk.co.mattgrundy.surfspotfinder.lib.repository.persistence.dao.PlacesDao


@Database(
    entities = [
        PlaceResult::class,
        PlacePhoto::class,
        PlaceReview::class
    ],
    version = 5
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun placesDao(): PlacesDao
}

val MIGRATION_1_2: Migration = object : Migration(1, 2) {
    override fun migrate(database: SupportSQLiteDatabase) {
        // Since we didn't alter the table, there's nothing else to do here.
    }
}