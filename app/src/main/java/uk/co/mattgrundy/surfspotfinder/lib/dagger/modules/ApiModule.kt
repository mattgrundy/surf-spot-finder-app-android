package uk.co.mattgrundy.surfspotfinder.lib.dagger.modules

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import uk.co.mattgrundy.surfspotfinder.lib.repository.network.api.PlacesApi
import javax.inject.Singleton

/**
 * API module for returning API facades
 */
@Module
class ApiModule {

    /**
     * Create a Retrofit instance of the Places API interface
     * @param retrofit Retrofit instance
     * @return Places API interface
     */
    @Provides
    @Singleton
    fun providePlacesApi(retrofit: Retrofit): PlacesApi {
        return retrofit.create(PlacesApi::class.java)
    }

}