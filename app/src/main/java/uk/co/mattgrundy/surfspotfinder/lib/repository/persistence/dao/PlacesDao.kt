package uk.co.mattgrundy.surfspotfinder.lib.repository.persistence.dao

import androidx.room.*
import uk.co.mattgrundy.surfspotfinder.lib.data.models.entities.PlaceEntity
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.PlacePhoto
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.PlaceResult
import uk.co.mattgrundy.surfspotfinder.lib.data.models.place.reviews.PlaceReview

@Dao
interface PlacesDao {
    @Query("SELECT * from place_result ORDER BY id ASC")
    fun getAllPlaces(): List<PlaceEntity>

    @Query("SELECT * from place_result WHERE place_id = :id AND hasFetchedDetails = 1 AND lastRefreshTime > :maxAge")
    fun getPlace(id: String, maxAge: Long): PlaceEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(placeResult: PlaceResult)

    @Query("DELETE FROM place_result")
    suspend fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(placeResults: Array<PlaceResult>) {
        for (placeResult in placeResults) {
            insert(placeResult)

            placeResult.photos?.let {
                for (photo in it) {
                    photo.placeId = placeResult.id
                    insert(photo)
                }
            }
        }
    }

    @Transaction
    fun insertPlace(placeResult: PlaceResult) {
        insert(placeResult)

        placeResult.photos?.let {
            for (photo in it) {
                insert(photo)
            }
        }

        placeResult.reviews?.let {
            for (review in it) {
                insert(review)
            }
        }
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(placePhoto: PlacePhoto)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(placeReview: PlaceReview)

}