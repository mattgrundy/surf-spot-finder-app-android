package uk.co.mattgrundy.surfspotfinder.lib.dagger.modules

import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import uk.co.mattgrundy.surfspotfinder.lib.config.CurrentEnvironment
import java.util.*
import javax.inject.Singleton

/**
 * Dagger module for network related tasks
 */
@Module
class NetworkModule {

    /**
     * Provides the Retrofit object
     * @param environment current defined app environment
     * @param client OkHttpClient instance
     * @param moshi Moshi JSON deserialiser instance
     * @return Retrofit instance
     */
    @Provides
    fun provideRetrofitInterface(
        environment: CurrentEnvironment.Environment,
        client: OkHttpClient,
        moshi: Moshi
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(environment.baseUrl)
            .client(client)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
    }

    /**
     * Provides OkHttpClient. Sets a Google API key instance on all requests provided by environment
     * @param interceptor OkHttp logging interceptor
     * @param environment current defined app environment
     * @return OkHttpClient instance
     */
    @Provides
    @Singleton
    fun provideOkHttpClient(
        interceptor: HttpLoggingInterceptor,
        environment: CurrentEnvironment.Environment
    ): OkHttpClient {
        val builder = OkHttpClient.Builder()
        builder.addInterceptor(interceptor)

        val googleApiKey = environment.googleApiKey

        builder.addInterceptor { chain: Interceptor.Chain ->
            val url = chain.request().url()
                .newBuilder()
                .addQueryParameter("key", googleApiKey)
                .build()

            val request: Request = chain.request()
                .newBuilder()
                .url(url)
                .build()

            chain.proceed(request)
        }

        return builder.build()
    }

    /**
     * Provides Moshi deserialiser instance
     * @return Moshi instance
     */
    @Provides
    fun provideMoshi(): Moshi {
        return Moshi.Builder()
            .add(Date::class.java, Rfc3339DateJsonAdapter())
            .add(KotlinJsonAdapterFactory())
            .build()
    }

    /**
     * Provides OkHttpLoggingInterceptor
     * Sets log level to BODY if current environment is a test environment
     * @param environment current defined app environment
     * @return HttpLoggingInterceptor instance
     */
    @Provides
    fun provideHttpLoggingInterceptor(
        environment: CurrentEnvironment.Environment
    ): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        if (environment.isTestEnvironment) {
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        }
        return httpLoggingInterceptor
    }

}