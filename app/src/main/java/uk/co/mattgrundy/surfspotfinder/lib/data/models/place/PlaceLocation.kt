package uk.co.mattgrundy.surfspotfinder.lib.data.models.place

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "place_location")
data class PlaceLocation(
    var lat: Double = .0,
    var lng: Double = .0
)