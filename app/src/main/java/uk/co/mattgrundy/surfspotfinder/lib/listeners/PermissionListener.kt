package uk.co.mattgrundy.surfspotfinder.lib.listeners

import android.content.pm.PackageManager
import androidx.fragment.app.Fragment

class PermissionListener {
    public var onPermissionListener: OnPermissionChanged? = null

    /**
     * Observer permission changes with a callback for results
     */
    fun observePermission(
        fragmentContext: Fragment,
        permissionType: String,
        onPermissionListenerInstance: OnPermissionChanged,
        requestCode: Int = 0
    ) {
        onPermissionListener = onPermissionListenerInstance

        // check if we've already got this permission
        if (checkPermission(fragmentContext, permissionType)) {
            onPermissionListener?.permissionChange(permissionType, true)
            return
        }

        // no location permission currently
        onPermissionListener?.permissionChange(permissionType, false)

        requestPermission(fragmentContext, permissionType, requestCode)
    }

    fun requestPermission(
        fragmentContext: Fragment,
        permissionType: String,
        requestCode: Int = 0
    ) {
        // request the permission
        fragmentContext.requestPermissions(
            arrayOf(permissionType),
            requestCode
        )
    }

    /**
     * Called from the context requesting the permission
     */
    fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        onPermissionListener?.let { permissionListener ->
            for ((index, permission) in permissions.withIndex()) {
                permissionListener.permissionChange(
                    permission,
                    grantResults[index] == PackageManager.PERMISSION_GRANTED
                )
            }
        }
    }

    /**
     * Check the permission status
     */
    private fun checkPermission(fragmentContext: Fragment, permissionType: String): Boolean {
        return fragmentContext.activity?.checkSelfPermission(permissionType) == PackageManager.PERMISSION_GRANTED
    }

    interface OnPermissionChanged {
        fun permissionChange(permissionType: String, granted: Boolean)
    }
}