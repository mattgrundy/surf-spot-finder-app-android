package uk.co.mattgrundy.surfspotfinder.lib.dagger.components

import android.app.Application
import androidx.fragment.app.FragmentFactory
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import uk.co.mattgrundy.surfspotfinder.MyApplication
import uk.co.mattgrundy.surfspotfinder.lib.dagger.modules.*
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        RepositoryModule::class,
        DatabaseRepository::class,
        ApiModule::class,
        NetworkModule::class,
        EnvironmentModule::class,
        ViewModelModule::class,
        FragmentInjectorModule::class,
        ListenerModule::class,
        FragmentFactoryModule::class
    ]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun fragmentFactory(): FragmentFactory
    fun inject(app: MyApplication)
}
