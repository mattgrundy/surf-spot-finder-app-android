package uk.co.mattgrundy.surfspotfinder.lib.listeners

/**
 * Generic interface for item clicks
 */
interface OnListItemClickListener {
    fun onItemClick(position: Int)
}