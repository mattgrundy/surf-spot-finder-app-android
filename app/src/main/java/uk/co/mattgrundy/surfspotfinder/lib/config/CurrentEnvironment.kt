package uk.co.mattgrundy.surfspotfinder.lib.config

import android.content.Context
import uk.co.mattgrundy.surfspotfinder.BuildConfig
import uk.co.mattgrundy.surfspotfinder.R
import uk.co.mattgrundy.surfspotfinder.lib.data.models.location.LocationModel
import uk.co.mattgrundy.surfspotfinder.lib.exceptions.InvalidEnvironmentException

/**
 * Environment for the current build type
 * Returns environmental variables for the current build
 * Current environment is defined by default by the RELEASE_TYPE key in the gradle build config
 */
class CurrentEnvironment {
    companion object {
        private val environments = HashMap<String, Environment>();
        val defaultLocation = LocationModel(55.3781, -3.4360)

        // initialise a static list of environments
        init {
            init()
        }

        fun init(){
            // issue with running MockWebServer over HTTPS - allowed in network_security_config for local only
            environments["TEST"] = Environment(
                true,
                "http://localhost:8080/",
                "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=%s&key=%s"
            )
            environments["DEV"] = Environment(
                true,
                "https://maps.googleapis.com/maps/api/",
                "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=%s&key=%s"
            )
            environments["RELEASE"] = Environment(
                false,
                "https://maps.googleapis.com/maps/api/",
                "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=%s&key=%s"
            )
        }

        /**
         * Get the environment for the given environment type
         * @param releaseTypeKey Description of the current release type
         * @return Environment containing the params for the release type
         */
        fun getEnvironment(
            releaseTypeKey: String = getReleaseTypeKey()
        ): Environment {
            if (!environments.containsKey(releaseTypeKey)) {
                throw InvalidEnvironmentException(
                    "$releaseTypeKey is not a defined environment"
                )
            }
            return environments[releaseTypeKey]!!
        }

        /**
         * Get the key for the release type from the BuildConfig
         * @return Release type usually one of [RELEASE, DEV, TEST]
         */
        fun getReleaseTypeKey(): String {
            return BuildConfig.RELEASE_TYPE
        }
    }

    /**
     * Class to provide environmental params for the current build type
     */
    class Environment(
        val isTestEnvironment: Boolean,
        val baseUrl: String,
        private val imageUrl: String
    ) {
        lateinit var googleApiKey: String

        fun initEnvironmentFromContext(context: Context) {
            googleApiKey = getApiKey(context)
        }

        /**
         * Get the image URL format and replace the given placeholders
         * @param googleImageID ID of the image to get
         * @return Formatted URL string for the image
         */
        fun getImageUrl(googleImageID: String): String {
            if(!this::googleApiKey.isInitialized){
                throw EnvironmentNotInitializedException()
            }

            return String.format(imageUrl, googleImageID, googleApiKey)
        }

        /**
         * Get the Google API key
         * @param context Current context
         * @return the Google API key
         */
        fun getApiKey(context: Context): String {
            return context.resources.getString(R.string.google_api_key)
        }

        class EnvironmentNotInitializedException : IllegalStateException(
            "Environment not initialized - must call initEnvironmentFromContext"
        )
    }
}
