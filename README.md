# Surf Spot Finder App
## General
Surf Spot Finder 🏄‍♂️, as its name would not suggest, is an app to find lodges from Google Places that are around you, or use the map to find places at another location.

Use case - find me hostels around `Newquay`
   - Click the map tab
   - Enter the keyword `hostel` in the search bar
   - Move the map over to Newquay area

Google API key is defined in the strings file under the key `google_api_key`.

## Running the app
The app is build with Gradle 3.6.2 and uses Java 1.8.

- The app should be run under the `debug` build variant
- Tests should be run under the `staging` build variant

## Structure
- `MVVM` structure
   - View -> ViewModel -> Repository -> API Service
      - View handles displaying any data to the user
      - View models handles holding reference to any data displayed in the view
      - Repository provides an interface for accessing the data (currently just via HTTP API, but project could be extended to add persistence resource here (see - extending the project))
      - API service provides the data from a remote API via HTTP
- `ViewModels` contain LiveData instances, using the observer pattern these instances respond to data changes by posting and observing data changes. For example the search -> display flow is:

  1. Posting search results changes, such as location change events from the location listener callback or map movement, or from search text changes. Changes are observed in `SurfSpotFinderContainerFragment` and trigger a data fetch from the repository
  2. The repository fetches the data and posts the data change to the ViewModel
  3. Both the listing and map fragments observe the data change to display the same data in their respective formats

I chose to use use ViewModels over other observer pattern frameworks, such as RxJava, because it handles lifecycle events automatically removing boilerplate code and simpler integration for this app's needs.

- `Dagger` dependency injection. Dependency injection is used for all key frameworks throughout. In addition to this, factories are used to allow us to instantiate instances that would usually be handled by the Android system:
   - `FragmentFactory` - allows control of Fragment construction and use constructor injection, making overriding in tests cleaner and easier
   - `ViewModelFactory` - similar to FragmentFactory, we use a factory to provide these so we can construct these with injection

### 3rd Party Libraries (all issued under Apache 2 licence)
- Dagger2 - used for dependency injection
- Glide - used for image fetching and caching
- Timber - used for tidy logging
- Mockk - used for mocking objects in tests
- Retrofit + OkHttp - Provides networking layer, inc. API facade and networking handling
- Moshi - Used by Retrofit for JSON deserialisation
- \+ a host of Google libraries

## Tests
- Provides examples of a few different styles of test:
   - Unit tests
      - e.g. `CurrentEnvironmentTest`
   - Integration tests
      - e.g. `PlaceDetailsViewModelTest` - mock repository responses to interact with a ViewModel
      - e.g. `NetworkRequestsTest` - MockWebServer to return static responses from API layer
   - UI tests - e.g. `ListPlacesUiTest` using Expresso to check the UI lists the data as expected. Here we use the `launchFragmentInContainer` function to test the fragment in isolation.
- Tests use both `CountDownLatch` and `IdleRegistry` as synchronisation aids (in `NetworkRequestsTest` and `ListPlacesUiTest` respectively). In both cases `IdleRegistry` is the cleaner option as we're able to use the same injected `OkHttp` client and register the idle resource in the test. Usually `IdleRegsitry` usage involves changes to production code which isn't as clean, so `CountDownLatch` is included as alternative example.
- `MockWebServer` for test reliability and repeatability. Allows use of the same static test data, to make sure remote API result changes aren't effecting our tests.

## Extending the project
- As part of the repository layer, use a persistence library to cache results to save repeated network calls, for example Room to store data in a local database instance
- Map movements, such as tapping a marker, sometimes causes unwanted map refreshes. I've used a basic map zoom scale factor to try to make it more reactive at higher zooms, but it could do with a little tweaking. Alternatively, find only changed markers to only re-draw these markers on map movement
- I've tried to include a range of tests as examples, but extending the tests for more complete code coverage and usage scenarios

## Fixing Performance Issues
I generally use the Android Studio Profile suite for debugging performance issue within an app. Typically I use 3 main areas:

- Memory Monitor

I would monitor memory usage while using the app and watch for creeping increased memory usage. To find a specific memory leak, I'd open the offending resource (usually a fragment or activity), close it, force a garbage collection from the profiler and then dump the heap and look for references to the closed resource. Usually memory leaks are caused by holding references to activity contexts. These can be overcome by releasing activity references when they are no longer needed (typically in lifecycle events such as onDestroy), or by using lifecycle aware components (such as ViewModels).

- CPU Monitor

Looking at CPU usage while using the app, I'd look for prolonged areas of high CPU usage. Typically, this is caused by high levels of overdraw in views (fixed by reducing complexity of layouts) or CPU intensive operations (for example bitmap manipulations which could be moved to a separate thread).

- Networking Monitor

I'd use the network monitor to see network resources that are slow to load, causing delays in the UI. This can be resolved by reducing the response time of the remote API or reducing the size of the resources.
